FROM ubuntu:20.10 as os-update
MAINTAINER Brad Aagaard <baagaard@usgs.gov>

RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends \
      g++ \
      make \
      file \
      automake \
      autoconf \
      libtool \
      curl \
      python3-dev \
      libpython3.7 \
      python3-setuptools \
      python3-numpy \
      python3-coverage \
      cython3 \
      zlib1g-dev \
      unzip \
      git \
      ca-certificates \
      libcppunit-dev \
      libhdf5-dev \
      python3-h5py \
      sqlite3 \
      libsqlite3-0 \
      libsqlite3-dev \
      lcov \
      valgrind \
      vim-common \
      vim-runtime \
      vim-nox

COPY docker/certs/ /usr/local/share/ca-certificates
RUN update-ca-certificates

ENV PYTHON_VERSION 3
ENV HDF5_INCDIR /usr/include/hdf5/serial
ENV HDF5_LIBDIR /usr/lib/x86_64-linux-gnu/hdf5/serial

# ----------------------------------------
from os-update as build-deps

# Install dependencies to /opt/geomodelgrids/dependencies
# Build dependencies in /home/installer/src

# Create 'geomodelgrids-user' user
ENV GEOMODELGRIDS_USER geomodelgrids-user
RUN useradd --create-home --shell /bin/bash $GEOMODELGRIDS_USER

ENV PREFIX_DIR /opt/geomodelgrids
ENV DEPS_DIR ${PREFIX_DIR}/dependencies
WORKDIR $DEPS_DIR
RUN chown -R $GEOMODELGRIDS_USER $PREFIX_DIR \
  && chgrp -R $GEOMODELGRIDS_USER $PREFIX_DIR

ENV BUILD_DIR /home/installer
WORKDIR $BUILD_DIR
RUN chown $GEOMODELGRIDS_USER $BUILD_DIR \
  && chgrp $GEOMODELGRIDS_USER $BUILD_DIR

USER $GEOMODELGRIDS_USER
ENV PATH $PATH:$DEPS_DIR/bin
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:$DEPS_DIR/lib
ENV PYTHONPATH $PYTHONPATH:$DEPS_DIR/lib/python$PYTHON_VERSION/site-packages

# proj
ENV PROJ_VER 6.3.1
RUN curl -O https://download.osgeo.org/proj/proj-6.3.1.tar.gz \
    && curl -O https://download.osgeo.org/proj/proj-datumgrid-1.8.zip \
    && tar -xf proj-${PROJ_VER}.tar.gz \
    && unzip -o ${BUILD_DIR}/proj-datumgrid-1.8.zip -d ${BUILD_DIR}/proj-${PROJ_VER}/data/ \
    && mkdir $BUILD_DIR/proj-build

WORKDIR $BUILD_DIR/proj-build
RUN ../proj-${PROJ_VER}/configure --prefix=$DEPS_DIR SQLITE3_CFLAGS=-I/usr/include SQLITE3_LIBS="-L/usr/lib -lsqlite3" \
  && make -j$(nproc) && make install


# ----------------------------------------
from build-deps as clean

USER root
RUN rm -fr $BUILD_DIR && unset BUILD_DIR
RUN apt-get clean

# Setup user and environment
USER $GEOMODELGRIDS_USER
WORKDIR /home/$GEOMODELGRIDS_USER


CMD /bin/bash
