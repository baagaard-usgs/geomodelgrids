# C Serial API

All functions in the serial C API are prefixed with `geomodelgrids_`.

* [Query](serial-query.html) Functions for querying values of a model at a point.
* [ErrorHandler](utils-errorhandler.html) Functions for accessing error handler information.
