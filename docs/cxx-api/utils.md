# C++ Utilities API

All classes in the utilities C++ API are in the `geomodelgrids::utils` namespace.

Classes

* [CRSTransformer](utils-crstransformer.html) Transform from one CRS to another.
* [ErrorHandler](utils-errorhandler.html) Error handler object.
* [TestDriver](utils-testdriver.html) Application driver for C++ unit tests using CppUnit.
