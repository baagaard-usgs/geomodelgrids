# Installation

**:TODO:**: Finish instructions after building binary.

## Installing via binary package

## Installing via source

### Dependencies

* C/C++ compiler

* autotools (automake, autoconf, libtool)

* HDF5 library (version 1.10.0 or later)

* Proj.4 (version 6.3.0 or later)

* Python 3 (if generating models)

  + h5py
  + numpy

### Downloading the source code

### Running configure

### Building and installing

### Running tests
